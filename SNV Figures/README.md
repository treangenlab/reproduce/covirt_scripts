The data used to generate the figures can be found on GHO at the following locations:
* Mason/NYC data -> /home/Users/ns58/SNV-analysis/SNV_calling_lowfreq_v2_0.02
* Baylor/Houston data -> /home/Users/ns58/SNV-analysis/Baylor_data_filtered_0.02
* GISAID SNP VCF -> /home/Users/ns58/SNV-analysis/gisaid_all_041820_complete_and_simulated_SNV.vcf
* Australia ONT RNA sequencing data -> /home/Users/ns58/SNV-analysis/PRJNA608224/vcf_files_lofreq_0.02
* SARS data -> /home/Users/ns58/SNV-analysis/SARS/vcf_files_lofreq_0.02
* MERS data -> /home/Users/ns58/SNV-analysis/MERS/vcf_files_lofreq_0.02

In order to create figures we simply run the Jupyter notebook provided.

Python modules used in the notebook:
* Pandas
* Numpy
* Matplotlib
* pyUpSetPlot