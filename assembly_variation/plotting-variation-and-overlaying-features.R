library(ggplot2)

setwd("~/Documents/COVID-19/manuscripts/Sapoval-diversity")

mafft_var_tab <- read.table("all-Sapoval-et-al-GISAID-and-ref-mafft-align-variation.tsv", sep="\t", header=T)
  # filtering out high variability at start and end that results from too few non-gap characters in the alignment
    # got filtering start and stop positions from `ad-hoc-get-start-and-stop-pos.py`
filtered_mafft_var_tab <- mafft_var_tab
filtered_mafft_var_tab$variation[filtered_mafft_var_tab$position <= 311 | filtered_mafft_var_tab$position >= 32838] <- 0

ggplot(data=filtered_mafft_var_tab, aes(x=position, y=variation)) + geom_line(color="darkblue", size=0.1) +
  theme_bw() + theme(panel.grid.major.x = element_blank(), panel.grid.minor.x = element_blank(), panel.grid.minor.y = element_blank()) +
  labs(x="Position", y="Variation (Shannon uncertainty)", title="Variation at aligned nucleotide positions of 6,929 SARS-CoV-2 genome assemblies") +
  ylim(0,1)

pdf("variation-across-sars-cov-2-assemblies.pdf", width=10, height=5)
ggplot(data=filtered_mafft_var_tab, aes(x=position, y=variation)) + geom_line(color="darkblue", size=0.1) +
  theme_bw() + theme(panel.grid.major.x = element_blank(), panel.grid.minor.x = element_blank(), panel.grid.minor.y = element_blank()) +
  labs(x="Position", y="Variation (Shannon uncertainty)", title="Variation at aligned nucleotide positions of 6,929 SARS-CoV-2 genome assemblies") +
  ylim(0,1)
dev.off()

## reading in feature positions from fig 2D
gene_positions_rel_to_ref_tab <- read.table("gene-positions-on-ref.tsv", sep="\t", header=T)

# mapping table here generated with `ad-hoc-map-ref-positions-to-alignment.py`
position_map <- read.table("ref-to-alignment-map.tsv", sep="\t", header=T)

# function for mapping ref positions to alignment positions
transform_positions <- function(ref_position_tab, map_tab) {

  aln_start <- vector()
  for ( position in ref_position_tab$start ) {

    new_pos <- map_tab[map_tab$ref_pos == position, "align_pos"]
    aln_start <- c(aln_start, new_pos)

  }

  aln_stop <- vector()
  for ( position in ref_position_tab$stop ) {

    new_pos <- map_tab[map_tab$ref_pos == position, "align_pos"]
    aln_stop <- c(aln_stop, new_pos)

  }

  return(data.frame("feature"=ref_position_tab$feature, "start"=aln_start, "stop"=aln_stop))

}

gene_positions_relative_to_msa_tab <- transform_positions(gene_positions_rel_to_ref_tab, position_map)

# plotting gene positions as rough sketch that i'll then overlay and beautify in affinity designer
# making vector of start and stop values interleaved
start_and_stop_positions <- c(rbind(gene_positions_relative_to_msa_tab$start, gene_positions_relative_to_msa_tab$stop))

# making arbitrary column for stacking features for easier manual manipulation
values <- rep(c(1,1,2,2), length(start_and_stop_positions) / 4)

# making color vector for start and stop
cols <- rep(c("start", "stop"), length(start_and_stop_positions) / 2)
# sticking together
features_to_plot <- data.frame("x"=start_and_stop_positions, "y"=values, "col"=cols)

pdf("features-overlay-raw.pdf", width=10, height=5)
ggplot(features_to_plot, aes(x=x, y=y, color=col)) + geom_point() + theme_bw()
dev.off()

## joined and beautified manually in affinity designer
