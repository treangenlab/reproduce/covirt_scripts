#!/usr/bin/env python

from Bio import SeqIO
import pandas as pd
import argparse
import sys

parser = argparse.ArgumentParser(description='Given the alignment of just a single sequence, this script returns a table mapping \
                                              the reference non-gap positions to the full alignment positions.')

required = parser.add_argument_group('required arguments')

required.add_argument("input_alignment", metavar='fasta_file', type=str, nargs="+", help="Input alignment file holding just 1 sequence.")
parser.add_argument("-o", "--output_tsv", help='Name of output tsv file (default: "ref-to-alignment-map.tsv").', action="store", dest="output_tsv", default="ref-to-alignment-map.tsv")

if len(sys.argv)==1:
    parser.print_help(sys.stderr)
    sys.exit(0)

args = parser.parse_args()

for seq in SeqIO.parse(open(args.input_alignment[0]), "fasta"):
    align_index = 1
    ref_index = 1
    
    align_index_list = []
    ref_index_list = []
    
    for base in seq.seq:
        
        if base != "-":
            align_index_list.append(align_index)
            ref_index_list.append(ref_index)
            ref_index += 1
        
        align_index += 1

df = pd.DataFrame({"ref_pos": ref_index_list, "align_pos": align_index_list})

df.to_csv(args.output_tsv, index=False, sep="\t")