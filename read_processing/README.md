All read datasets for MERS/SARS/Myl_CoV/HCoV can be found at:

```
/home/Users/yl181/nCov2019/datasets
```

The data can be downloaded directly from NCBI SRA database, the accession IDs of the datasets can be found in `/metadata`. This script includes a module for downloading the data.

This script is only designed to process Illumina paired-end datasets, and all datasets processed using this script are Illumina paired-end datasets.
For downloading and mapping certain Illumina paired-end datasets, the accession IDs of the datasets are required to store in the metadata file, in text format of one accession ID per line. The reference genome is required in fasta format.

Command for running the script:
```
python read_processing.py {reference.fasta} {metadata.txt} {output_dir}
```

An example of using the script:
```
python read_processing.py reference_files/MERS.fasta metadata/MERS.txt MERS_output
```

Required tools and python package:
* NCBI SRA toolkit
* Trimmomatic
* BWA-MEM
* Samtools