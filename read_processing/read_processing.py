"""This module is responsible for data retriveing, trimming and mapping"""
import os
import subprocess
import argparse

def read_mapping(sample_metadata, reference_file, num_cores, output_dir):
    '''map the reads to the reference'''
    sam_output_dir = os.path.join(output_dir, "sam_files")
    if not os.path.exists(sam_output_dir):
        os.mkdir(sam_output_dir)

    for sample in sample_metadata:
        subprocess.run([
            "bwa",
            "mem",
            "-t", str(num_cores),
            "-o", os.path.join(sam_output_dir, f"{sample}.sam"),
            reference_file,
            os.path.join("trimmed_reads", sample, f"{sample}_1.fastq"),
            os.path.join("trimmed_reads", sample, f"{sample}_2.fastq")],
                       stdout=open(os.path.join(output_dir, "simulation.log"), "a"),
                       stderr=open(os.path.join(output_dir, "simulation.err"), "a"),
                       check=True)

def data_retriving(sample_metadata, read_dir):
    '''download datasets from NCBI'''
    if not os.path.exists(read_dir):
        os.mkdir(read_dir)
    for run in sample_metadata:
        if not os.path.exists(os.path.join(read_dir, run)):
            subprocess.run(["prefetch", run],
                           check=True)
            if not os.path.exists(os.path.join(read_dir, run)):
                os.mkdir(os.path.join(read_dir, run))
            subprocess.run([
                "fastq-dump",
                "--split-3",
                "--outdir", os.path.join(read_dir, run), run],
                           check=True)

def run_trimmomatic(illumina_sample_list, adapter_file, num_cores, read_dir):
    '''trim reads with universal adapters'''
    if not os.path.exists("trimmed_reads"):
        os.mkdir("trimmed_reads")
    for sample in illumina_sample_list:
        if not os.path.exists(f"trimmed_reads/{sample}"):
            os.mkdir(f"trimmed_reads/{sample}")
        subprocess.run(["trimmomatic",
                        "PE",
                        "-quiet",
                        "-threads", str(num_cores),
                        f"{read_dir}/{sample}/{sample}_1.fastq",
                        f"{read_dir}/{sample}/{sample}_2.fastq",
                        "-baseout", f"trimmed_reads/{sample}/{sample}_trimmed.fastq",
                        f"ILLUMINACLIP:{adapter_file}:2:30:10",
                        "LEADING:3",
                        "TRAILING:3",
                        "SLIDINGWINDOW:4:15",
                        "MINLEN:36"],
                       check=True)

        subprocess.run(["mv",
                        f"trimmed_reads/{sample}/{sample}_trimmed_1P.fastq",
                        f"trimmed_reads/{sample}/{sample}_1.fastq"],
                       check=True)
        subprocess.run(["mv",
                        f"trimmed_reads/{sample}/{sample}_trimmed_2P.fastq",
                        f"trimmed_reads/{sample}/{sample}_2.fastq"],
                       check=True)

def sort_samfile(sample_metadata, output_dir, num_cores):
    '''converting and sorting alignment files'''
    bam_files = os.path.join(output_dir, "bam_files")
    sam_files = os.path.join(output_dir, "sam_files")
    if not os.path.exists(bam_files):
        os.mkdir(bam_files)

    for sample in sample_metadata:
        # covert sam file to binary bam file
        subprocess.run([
            "samtools",
            "view",
            "-@", str(num_cores),
            "-bS", os.path.join(sam_files, f"{sample}.sam"),
            "-o", os.path.join(bam_files, f"{sample}.bam")],
                       check=True)
        # sort the bam file for variant caller
        subprocess.run([
            "samtools",
            "sort",
            "-@", str(num_cores),
            "-o", os.path.join(bam_files, f"{sample}_sort.bam"),
            "-O", "BAM", os.path.join(bam_files, f"{sample}.bam")],
                       check=True)
        # indexing the sorted bam file for variant caller
        subprocess.run([
            "samtools",
            "index",
            os.path.join(bam_files, f"{sample}_sort.bam")],
                       check=True)

def main():
    '''main function'''
    parser = argparse.ArgumentParser(description="Read Processing")
    parser.add_argument("reference", type=str, help="input reference genome in fasta format")
    parser.add_argument("metadata", type=str, help="metadata which stores the accession number")
    parser.add_argument("output", type=str, help="output directory")
    parser.add_argument("-numcore", type=int, default=60,
                        help="Number of processor used for parallelization")
    parser.add_argument("-adapters", type=str, default="adapters.fa",
                        help="Adapters for trimming")

    args = parser.parse_args()

    num_cores = args.numcore
    adapter_file = args.adapters
    output_dir = args.output

    reference_file = args.reference
    metadata_file = args.metadata

    sample_metadata = []
    with open(metadata_file, "r") as input_f:
        lines = input_f.readlines()
        for line in lines:
            sample_metadata.append(line.strip())

    if not os.path.exists(output_dir):
        os.mkdir(output_dir)
    read_dir = "reads"

    data_retriving(sample_metadata, read_dir)
    run_trimmomatic(sample_metadata, adapter_file, num_cores, read_dir)
    subprocess.call(["bwa", "index", reference_file])
    subprocess.call(["samtools", "faidx", reference_file])
    read_mapping(sample_metadata, reference_file, num_cores, output_dir)
    sort_samfile(sample_metadata, output_dir, num_cores)

if __name__ == "__main__":
    main()
