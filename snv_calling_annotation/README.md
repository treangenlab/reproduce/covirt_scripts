This script takes the output files of read_processing.py

It's better to put this script and read_processing.py in the same directory since reference files are shared by both scripts.
The accession IDs of the datasets are required to store in the metadata file, in text format of one accession ID per line. The reference genome is required in fasta format.

A snpEff database of the reference genome must be prebuild before running the pipeline. To build a snpEff database, please refer to http://snpeff.sourceforge.net/SnpEff_manual.html#databases

Command for running the script, output_dir must match the output directory used in `read_processing.py`, and by default, the script does not run minimum allele frequency filter unless `-min-af` is set:
```
python snv_calling_annotation.py {reference.fasta} {metadata.txt} {snpEff_database} {output_dir}
```

An example of using the script:
```
python snv_calling_annotation.py reference_files/MERS.fasta metadata/MERS.txt MERS_snpEff_database MERS_output -min-af 0.5
```

Required tools and python package:
* Lofreq
* SnpEff
* PyVCF