"""This module is responsible for data vcf generation, annotation and filtering"""
import os
import subprocess
import argparse
import vcf

def variant_call_lofreq(mapped_list, reference_file, output_dir, num_cores):
    '''variant calling using loFreq'''
    bam_files = os.path.join(output_dir, "bam_files")
    vcf_files = os.path.join(output_dir, "vcf_files_lofreq")
    if not os.path.exists(vcf_files):
        os.mkdir(vcf_files)

    for sample in mapped_list:
        if os.path.exists(os.path.join(vcf_files, f"{sample}.vcf")):
            os.remove(os.path.join(vcf_files, f"{sample}.vcf"))
        subprocess.run([
            "lofreq",
            "call-parallel",
            "--pp-threads", str(num_cores),
            "-f", f"{reference_file}",
            "-o",
            os.path.join(vcf_files, f"{sample}.vcf"),
            os.path.join(bam_files, f"{sample}_sort.bam")],
                       stdout=open(os.path.join(output_dir, "simulation.log"), "a"),
                       stderr=open(os.path.join(output_dir, "simulation.err"), "a"),
                       check=True)

def variant_analysis(sample_metadata, database, output_dir, vcf_dir):
    '''variant annotating using snpEff'''
    variant_analysis_files = os.path.join(output_dir, "variant_analysis")
    variant_analysis_summary_files = os.path.join(output_dir, "variant_analysis_summary")
    vcf_files = os.path.join(output_dir, vcf_dir)
    if not os.path.exists(variant_analysis_files):
        os.mkdir(variant_analysis_files)
    if not os.path.exists(variant_analysis_summary_files):
        os.mkdir(variant_analysis_summary_files)

    for sample in sample_metadata:
        vcf_input = os.path.join(vcf_files, f"{sample}.vcf")
        if os.path.exists(vcf_input):
            vcf_reader = vcf.Reader(filename=vcf_input)
            count = 0
            for _ in vcf_reader:
                count += 1
            if count != 0:
                summary_output = os.path.join(variant_analysis_summary_files,
                                              f"{sample}_snpEff_summary.html")
                vcf_output = os.path.join(variant_analysis_files, f"{sample}.eff.vcf")
                os.system(f"snpEff eff -stats {summary_output} {database} {vcf_input} > {vcf_output}")

def variant_filtering(sample_metadata, output_dir, vcf_dir, filtered_dir, min_af_threshold):
    '''variant filtering using lofreq'''
    vcf_files = os.path.join(output_dir, vcf_dir)
    filtered_vcf_files = os.path.join(output_dir, filtered_dir)
    if not os.path.exists(filtered_vcf_files):
        os.mkdir(filtered_vcf_files)

    for sample in sample_metadata:
        vcf_input = os.path.join(vcf_files, f"{sample}.vcf")
        if os.path.exists(os.path.join(filtered_vcf_files, f"{sample}.vcf")):
            os.remove(os.path.join(filtered_vcf_files, f"{sample}.vcf"))
        if os.path.exists(vcf_input):
            subprocess.run([
                "lofreq",
                "filter",
                "-a",
                f"{min_af_threshold}",
                "-i",
                vcf_input,
                "-o",
                os.path.join(filtered_vcf_files, f"{sample}.vcf")],
                           check=True)

def main():
    '''main function'''
    parser = argparse.ArgumentParser(description="Read Processing")
    parser.add_argument("reference", type=str, help="input reference genome in fasta format")
    parser.add_argument("metadata", type=str, help="metadata which stores the accession number")
    parser.add_argument("effdata", type=str,
                        help="SnpEff database associated with the reference")
    parser.add_argument("output", type=str, help="output directory")
    parser.add_argument("-numcore", type=int, default=60,
                        help="Number of processor used for parallelization")
    parser.add_argument("-min-af", type=float, default=0,
                        help="Minimux allele frequency for filtering vcf outputs, no filtering by default")

    args = parser.parse_args()

    num_cores = args.numcore
    output_dir = args.output
    reference_file = args.reference
    metadata_file = args.metadata
    snpeff_database = args.effdata
    min_af_threshold = args.min_af

    sample_metadata = []
    with open(metadata_file, "r") as input_f:
        lines = input_f.readlines()
        for line in lines:
            sample_metadata.append(line.strip())

    if not os.path.exists(output_dir):
        os.mkdir(output_dir)

    variant_call_lofreq(sample_metadata, reference_file, output_dir, num_cores)
    variant_analysis(sample_metadata, snpeff_database, output_dir, "vcf_files_lofreq")
    if min_af_threshold > 0:
        variant_filtering(sample_metadata,
                          output_dir,
                          "vcf_files_lofreq",
                          "vcf_files_lofreq_filtered",
                          min_af_threshold)

if __name__ == "__main__":
    main()
