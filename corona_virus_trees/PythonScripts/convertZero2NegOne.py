import sys

"""
INPUT:
    An iTOL file that has binary data (using 1 and 0) and converting it into (1
    and -1). 

OUTPUT:
    The corrected file is printed to stdout.

USAGE:
    python PythonScripts/changer.py itol_metadata_files/itol_input_files/convertZero2NegOne.py
"""

file1 = open(sys.argv[1]).readlines()

for line in file1:
    linearray = line.split(",")
    printarray = []
    if "GISAID_" in linearray[0] \
       or "SARS" in linearray[0] \
       or "COV" in linearray[0]:
        name_temp = linearray[0]
        for value in linearray[1:]:
            if value == '1':
                printarray.append('1')
            else:
                printarray.append('-1')
        printarray_string = ','.join(printarray)
        print(f"{name_temp},{printarray_string}")
    else:
        print(line)

