# Creating Corona Virus i-TOL phylogentic Tree
# Dreycey Albin

## files that can be used with the trees (itol_metadata_files/itol_input_files/)
* Continent_colorlabel_template.txt   
* SNP_color_converted.txt             
* colorCountry_FILE_practice.txt      
* SNP_binary.txt                      
* SNV_color_converted.txt             
* mutation_heatmap_FILE_practice.txt 

**NOTE**: The SNV_color_converted.txt can only be used with the tree containing
Mason Lab data (fulltree_04292020). 

## How to create different iTOL input files 
### 1. Creating color ring for Continent.
To create a file with the continents added, the following scripts may be used:

1. Make a copy of the template to use.
```
cp itol_metadata_files/itol_template_files/Continent_colorlabel.txt continent_coloring.txt
```

2. Add colored continents.
```
python PythonScripts/continentcolor.py Corona_trees-metadata/gisaid_metadata.csv continent_coloring.txt
```

3. Drag and drop into iTOL viewer. 

### 2. Creating rings for SNPs.
This is done using the DATASET_BINARY type of file allowede by iTOL. For this
file type, manually copy over the binary output data, then convert and existing
zeros present into -1: 
                                           
1. Make a copy of the template to use.
```
cp itol_metadata_files/itol_template_files/SNV_color.txt SNV_color_testing.txt
```
Then manually add the data.

2. Turn 0s into -1
```
python PythonScripts/convertZero2NegOne.py SNV_color_testing.txt
```

3. Drag and drop into iTOL viewer.

### 3. Creating rings for SNVs.
Follow the same directions for SNPs. 

### Use a VCF to generate a heatmap and find top 13 SNPs/SNVs.
In addition to manually adding the data, there are also scripts available that
allow one to gather the top SNPs from an input VCF file. The following script
and method may be modifed to do this: 

```
python PythonScripts/mutation_heatmap.py \
       Corona_trees-metadata/parsnp.eff.vcf \
       itol_metadata_files/itol_template_files/mutation_heatmap_FILE.txt
```
## Directories

### Corona_trees-metadata/
This directory contains information regarding the different newick trees
associated with the aligned genomes resulting from the multiple genome
alignment. There are also files related to the metadata linked to the GISAID
metadata. 

* GISAID_parsnp.tree (Only GISAID genomes)
* fulltree_04292020 (All genomes)

### PhyloFigures/
This directory contains all of the figures related to the phylogentic plots. 

### PythonScripts/
This directory contains the python scripts related to the processing for getting
the iTOL plots ready.

### example_iTOL_files/
These are files given by iTOL that give indication of what figures are possible. 

### itol_metadata_files/
This directory contains the files that can be input into iTOL for visualizing
information related to each sample, such as SNPs, SNVs, and continent of origin.


