DATASET_BINARY







SEPARATOR COMMA







DATASET_LABEL, SNV Presence (Top 15)







COLOR,#ff0000







#1: rectangle; 2: circle; 3: star; 4: right pointing triangle



#5: left pointing triangle





FIELD_LABELS,(26729 'T' 'C'),(14786 'C' 'T'),(14408 'C' 'T'),(18736 'T' 'C'),(17858 'A' 'G'),(3037 'C' 'T'),(28253 'C' 'T'),(1059 'C' 'T'),(25469 'C' 'T'),(25563 'G' 'T'),(28606 'C' 'T'),(17747 'C' 'T'),(28144 'T' 'C'),(23403 'A' 'G'),(18060 'C' 'T')

FIELD_COLORS,#972742,#7F6963,#87633A,#577B7D,#07875D,#9E2B11,#7C6D5D,#596A52,#3A613F,#93521A,#823600,#023F57,#111A32,#3A541D,#141111



FIELD_SHAPES,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1



#LEGEND_TITLE,SNV Presence (Top 15)

#LEGEND_SHAPES,1

#LEGEND_LABELS,SNVs

#LEGEND_COLORS,#CCCC00



SHOW_INTERNAL,0

MARGIN,0

HEIGHT_FACTOR,15

SYMBOL_SPACING,10







DATA



SARS-CoV-2-ref.gbk.fna,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1
COV-20200313-P4-H03-P_S19.fasta,-1,-1,1,-1,-1,1,-1,1,-1,1,-1,-1,-1,1,-1
COV-20200315-P10-B08-P.fasta,-1,-1,1,-1,-1,1,-1,1,-1,-1,-1,-1,-1,1,-1
COV-20200315-P10-H01-P.fasta,-1,-1,1,-1,-1,1,-1,1,-1,1,-1,-1,-1,1,-1
COV-20200313-P4-B06-P_S23.fasta,-1,-1,1,-1,-1,1,-1,1,-1,1,-1,-1,-1,1,-1
COV-20200314-P9-A08-P.fasta,-1,-1,1,-1,-1,1,-1,-1,-1,1,-1,-1,-1,1,-1
COV-20200315-P10-B12-P.fasta,-1,-1,1,-1,-1,1,-1,1,-1,1,-1,-1,-1,1,-1
COV-20200313-P5-H02-P_S26.fasta,-1,-1,1,-1,-1,1,-1,1,-1,1,-1,-1,-1,1,-1
COV-20200315-P10-H04-P.fasta,-1,-1,1,-1,-1,1,-1,1,-1,1,-1,-1,-1,1,-1
COV-20200315-P10-H08-P.fasta,-1,-1,1,-1,-1,1,-1,-1,-1,1,-1,-1,-1,1,-1
COV-20200316-P12-G02-P.fasta,-1,-1,1,-1,-1,1,-1,1,-1,1,-1,-1,-1,1,-1
COV-20200315-P10-B11-P.fasta,-1,-1,1,-1,-1,1,-1,-1,-1,-1,-1,-1,-1,1,-1
COV-20200313-P5-A03-P_S27.fasta,-1,-1,-1,-1,-1,-1,-1,1,-1,1,-1,-1,-1,-1,-1
COV-20200316-P12-F01-P.fasta,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1
COV-20200315-P10-G07-P.fasta,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,1,-1
COV-20200316-P12-C04-P.fasta,-1,-1,1,-1,-1,1,-1,1,-1,1,-1,-1,-1,1,-1
COV-20200315-P10-F10-P.fasta,-1,-1,1,-1,-1,1,-1,1,-1,1,-1,-1,-1,1,-1
COV-20200313-P4-D01-P_S18.fasta,-1,-1,1,-1,-1,1,-1,1,-1,1,-1,-1,-1,1,-1
COV-20200315-P10-F09-P.fasta,-1,-1,1,-1,-1,1,-1,1,-1,1,-1,-1,-1,1,-1
COV-20200313-P4-C01-P_S17.fasta,-1,-1,1,-1,-1,1,-1,1,-1,1,-1,-1,-1,1,-1
COV-20200312-P2-D02-P_S2.fasta,-1,-1,-1,-1,1,-1,-1,-1,-1,-1,-1,1,1,-1,-1
COV-20200315-P10-G05-P.fasta,-1,-1,1,-1,-1,1,-1,1,-1,1,-1,-1,-1,1,-1
COV-20200316-P12-A09-P.fasta,-1,-1,1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,1,-1
COV-20200316-P12-D09-P.fasta,-1,-1,1,-1,-1,1,-1,1,-1,1,-1,-1,-1,1,-1
COV-20200316-P11-C04-P.fasta,-1,-1,1,-1,-1,1,-1,1,-1,1,-1,-1,-1,1,-1
COV-20200312-P3-E02-P_S9.fasta,-1,-1,1,-1,-1,1,-1,1,-1,1,-1,-1,-1,1,-1
COV-20200312-P2-C03-P_S5.fasta,-1,-1,1,-1,-1,1,-1,1,-1,1,-1,-1,-1,1,-1
COV-20200316-P11-F06-P.fasta,-1,-1,1,-1,-1,1,-1,1,-1,1,-1,-1,-1,1,-1
COV-20200314-P9-E04-P.fasta,-1,-1,1,-1,-1,1,-1,1,-1,1,-1,-1,-1,1,-1
COV-20200316-P11-E09-P.fasta,-1,-1,1,-1,-1,1,-1,1,-1,1,-1,-1,-1,1,-1
COV-20200316-P11-B07-P.fasta,-1,-1,1,-1,-1,1,-1,-1,-1,-1,-1,-1,-1,1,-1
COV-20200315-P10-A12-P.fasta,-1,-1,1,-1,-1,1,-1,1,-1,1,-1,-1,-1,1,-1
COV-20200315-P10-F11-P.fasta,-1,-1,-1,-1,-1,1,-1,-1,-1,-1,-1,-1,-1,1,-1
COV-20200314-P9-B06-P.fasta,-1,-1,1,-1,-1,1,-1,1,-1,1,-1,-1,-1,1,-1
COV-20200315-P10-B09-P.fasta,-1,-1,-1,-1,-1,1,-1,1,-1,1,-1,-1,-1,1,-1
COV-20200314-P9-G04-P.fasta,-1,-1,1,-1,-1,1,-1,-1,-1,1,-1,-1,-1,1,-1
COV-20200315-P10-F06-P.fasta,-1,-1,1,-1,-1,1,-1,1,-1,1,-1,-1,-1,1,-1
COV-20200316-P12-G03-P.fasta,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1
COV-20200315-P10-F04-P.fasta,-1,-1,1,-1,-1,1,-1,-1,-1,1,-1,-1,-1,1,-1
COV-20200316-P12-E06-P.fasta,-1,-1,1,-1,-1,1,-1,1,-1,1,-1,-1,-1,1,-1
COV-20200314-P9-B08-P.fasta,-1,-1,1,-1,-1,1,-1,1,-1,1,-1,-1,-1,1,-1
COV-20200313-P4-E05-P_S22.fasta,-1,-1,1,-1,-1,1,-1,1,-1,1,-1,-1,-1,1,-1
COV-20200316-P11-A06-P.fasta,-1,-1,1,-1,-1,1,-1,1,-1,1,-1,-1,-1,1,-1
COV-20200315-P10-G04-P.fasta,-1,-1,1,-1,-1,-1,-1,1,-1,1,-1,-1,-1,1,-1
COV-20200316-P12-F07-P.fasta,-1,-1,1,-1,-1,1,-1,1,-1,1,-1,-1,-1,1,-1
COV-20200315-P10-G11-P.fasta,-1,-1,1,-1,-1,1,-1,1,-1,1,-1,-1,-1,1,-1
COV-20200316-P12-H01-P.fasta,-1,-1,1,-1,-1,1,-1,1,-1,1,-1,-1,-1,1,-1
COV-20200316-P11-G03-P.fasta,-1,-1,1,-1,-1,-1,-1,1,-1,1,-1,-1,-1,1,-1
COV-20200315-P10-G06-P.fasta,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1
COV-20200316-P12-E01-P.fasta,-1,-1,1,-1,-1,1,-1,-1,-1,-1,-1,-1,-1,1,-1
COV-20200314-P9-C06-P.fasta,-1,-1,-1,-1,-1,1,-1,1,-1,1,-1,-1,-1,1,-1
COV-20200315-P10-C03-P.fasta,-1,-1,1,-1,-1,1,-1,1,-1,1,-1,-1,-1,1,-1
COV-20200313-P4-D04-P_S20.fasta,-1,-1,-1,-1,1,-1,-1,-1,-1,-1,-1,1,1,-1,-1
COV-20200314-P9-D03-P.fasta,-1,-1,1,-1,-1,1,-1,1,-1,1,-1,-1,-1,1,-1
COV-20200316-P11-C06-P.fasta,-1,-1,-1,-1,-1,-1,-1,1,-1,1,-1,-1,-1,-1,-1
COV-20200316-P11-H03-P.fasta,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1
COV-20200316-P12-F03-P.fasta,-1,-1,1,-1,-1,1,-1,1,-1,1,-1,-1,-1,1,-1
COV-20200313-P5-F01-P_S24.fasta,-1,-1,1,-1,-1,1,-1,1,-1,1,-1,-1,-1,1,-1
COV-20200316-P12-C01-P.fasta,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1
COV-20200314-P9-B05-P.fasta,-1,-1,1,-1,-1,1,-1,1,-1,1,-1,-1,-1,1,-1
COV-20200316-P12-B04-P.fasta,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1
COV-20200315-P10-B03-P.fasta,-1,-1,1,-1,-1,1,-1,1,-1,1,-1,-1,-1,1,-1
COV-20200315-P10-E01-P.fasta,1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1
COV-20200315-P10-C04-P.fasta,-1,-1,1,-1,-1,1,-1,1,-1,1,-1,-1,-1,1,-1
COV-20200313-P4-G04-P_S21.fasta,-1,-1,1,-1,-1,1,-1,1,-1,1,-1,-1,-1,1,-1
COV-20200316-P12-G06-P.fasta,-1,-1,1,-1,-1,1,-1,1,-1,1,-1,-1,-1,1,-1
COV-20200313-P5-G03-P_S28.fasta,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1
COV-20200315-P10-E02-P.fasta,-1,-1,1,-1,-1,1,-1,1,-1,1,-1,-1,-1,1,-1
COV-20200316-P11-F07-P.fasta,-1,-1,1,-1,-1,1,-1,1,-1,1,-1,-1,-1,1,-1
COV-20200315-P10-D11-P.fasta,-1,-1,1,-1,-1,1,-1,1,-1,1,-1,-1,-1,1,-1
COV-20200315-P10-E08-P.fasta,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,1,-1
COV-20200316-P12-F02-P.fasta,-1,-1,1,-1,-1,1,-1,1,-1,1,-1,-1,-1,1,-1
COV-20200315-P10-D09-P.fasta,-1,-1,1,-1,-1,1,-1,1,-1,1,-1,-1,-1,1,-1
COV-20200314-P9-A05-P.fasta,-1,-1,-1,-1,1,-1,-1,-1,-1,-1,-1,1,1,-1,-1
COV-20200315-P10-G10-P.fasta,-1,-1,1,-1,-1,1,-1,1,-1,1,-1,-1,-1,1,-1
COV-20200316-P11-E05-P.fasta,-1,-1,1,-1,-1,1,-1,-1,-1,1,-1,-1,-1,1,-1
COV-20200316-P12-G08-P.fasta,-1,-1,1,-1,-1,1,-1,1,-1,1,-1,-1,-1,1,-1
COV-20200312-P2-H03-P_S7.fasta,-1,-1,1,-1,-1,1,-1,1,-1,1,-1,-1,-1,1,-1
COV-20200316-P11-B08-P.fasta,-1,-1,-1,-1,-1,-1,-1,-1,-1,1,-1,-1,-1,-1,-1
COV-20200316-P11-B03-P.fasta,-1,-1,1,-1,-1,1,-1,1,-1,1,-1,-1,-1,1,-1
COV-20200315-P10-D06-P.fasta,-1,-1,1,-1,-1,1,-1,1,-1,1,-1,-1,-1,1,-1
COV-20200316-P12-C09-P.fasta,-1,-1,1,-1,-1,1,-1,1,-1,1,-1,-1,-1,1,-1
COV-20200316-P11-B04-P.fasta,-1,-1,1,-1,-1,1,-1,1,-1,1,-1,-1,-1,1,-1
COV-20200316-P12-D04-P.fasta,-1,-1,1,-1,-1,1,-1,1,-1,1,-1,-1,-1,1,-1
COV-20200316-P12-D08-P.fasta,-1,-1,1,-1,-1,1,-1,1,-1,1,-1,-1,-1,1,-1
COV-20200316-P11-E06-P.fasta,-1,-1,-1,-1,-1,1,-1,1,-1,-1,-1,-1,-1,-1,-1
COV-20200312-P2-E02-P_S3.fasta,-1,-1,1,-1,-1,1,-1,-1,-1,1,-1,-1,-1,1,-1
COV-20200316-P12-D06-P.fasta,-1,-1,1,-1,-1,1,-1,1,-1,-1,-1,-1,-1,1,-1
COV-20200316-P12-E09-P.fasta,-1,-1,1,-1,-1,1,-1,1,-1,1,-1,-1,-1,1,-1
COV-20200315-P10-H11-P.fasta,-1,-1,1,-1,-1,1,-1,-1,-1,1,-1,-1,-1,-1,-1
COV-20200316-P11-C05-P.fasta,-1,-1,1,-1,-1,1,-1,1,-1,1,-1,-1,-1,1,-1
COV-20200316-P11-E04-P.fasta,-1,-1,1,-1,-1,1,-1,1,-1,1,-1,-1,-1,1,-1
COV-20200315-P10-A06-P.fasta,-1,-1,1,-1,-1,1,-1,1,-1,1,-1,-1,-1,1,-1
COV-20200315-P10-A02-P.fasta,-1,-1,1,-1,-1,1,-1,1,-1,1,-1,-1,-1,1,-1
COV-20200315-P10-C11-P.fasta,-1,-1,1,-1,-1,1,-1,-1,-1,1,-1,-1,-1,1,-1
COV-20200315-P10-G08-P.fasta,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1
COV-20200316-P11-F04-P.fasta,-1,-1,1,-1,-1,1,-1,1,-1,1,-1,-1,-1,1,-1
COV-20200314-P9-B09-P.fasta,-1,-1,1,-1,-1,1,-1,1,-1,1,-1,-1,-1,1,-1
COV-20200316-P12-D02-P.fasta,-1,1,1,-1,-1,1,-1,1,-1,1,-1,-1,-1,1,-1
COV-20200316-P12-G01-P.fasta,-1,1,1,-1,-1,1,-1,1,-1,1,-1,-1,-1,1,-1
COV-20200316-P12-F08-P.fasta,-1,-1,-1,-1,-1,-1,-1,1,-1,1,-1,-1,-1,-1,-1
COV-20200315-P10-A05-P.fasta,-1,-1,1,-1,-1,1,-1,1,-1,1,-1,-1,-1,1,-1
COV-20200315-P10-E07-P.fasta,-1,-1,1,-1,-1,1,-1,1,-1,1,-1,-1,-1,1,-1
COV-20200316-P11-C03-P.fasta,-1,-1,1,-1,-1,1,-1,1,-1,1,-1,-1,-1,1,-1
COV-20200316-P12-B07-P.fasta,-1,-1,1,-1,-1,1,-1,-1,-1,1,-1,-1,-1,1,-1
COV-20200316-P11-F09-P.fasta,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,1,-1
COV-20200316-P11-H06-P.fasta,-1,-1,1,-1,-1,1,-1,1,-1,1,-1,-1,-1,1,-1
COV-20200315-P10-E06-P.fasta,-1,-1,1,-1,-1,1,-1,1,-1,1,-1,-1,-1,1,-1
COV-20200315-P10-C09-P.fasta,-1,-1,1,-1,-1,1,-1,-1,-1,1,-1,-1,-1,1,-1
COV-20200312-P2-D04-P_S8.fasta,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,1,-1
COV-20200316-P11-A03-P.fasta,1,-1,-1,1,-1,-1,-1,-1,-1,-1,-1,-1,1,-1,-1
