# Files used for iTOL visualization

This directory contains the different files used for visualizing the country and
continent of origin for all of the samples a phylogenetic tree. In addition,
there are iTOL input files that can be used for visualizing the SNPs and SNVs
present for a given tree. 

## Description of subdirectories
### itol_input_files/    
This directory contains files that can be directly loaded into the iTOL tree
containing the corona virus pylo-tree. It should be noted that the SNV data will
only appear for the full tree (including Mason Lab samples). 

### itol_template_files/
This directory contains iTOL sample files that can be used with the python
scripts to create the full files needed for visualizing the metadata.
Essentially, the output of the python files can be appended to these template
files to create visualization files, as in the itol input files. 
