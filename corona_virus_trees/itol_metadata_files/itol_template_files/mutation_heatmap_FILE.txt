DATASET_HEATMAP
#In heatmaps, each ID is associated to multiple numeric values, which are displayed as a set of colored boxes defined by a color gradient
SEPARATOR COMMA

DATASET_LABEL,example_heatmap
COLOR,#ff0000

MARGIN,0
STRIP_WIDTH,25
SHOW_INTERNAL,0
SHOW_TREE,1
COLOR_MIN,#ff0000
COLOR_MAX,#0000ff
USE_MID_COLOR,1
COLOR_MID,#ffff00

#define labels for each individual field column

FIELD_LABELS,23403,3037,14408,25563,1059,11083,14805,26144,18060,17747,17858,22447,22444
DATA
